# 📝 Task Management Tool

Welcome to the Task Management Tool! This project is a simple web application to help you manage your tasks effectively. Developed using Flask, SQLAlchemy, and PostgreSQL, this tool allows you to register, log in, create, and manage tasks with ease.

## 🌟 Features

- 🔐 User Registration and Authentication
- 📋 Create, Read, Update, and Delete (CRUD) tasks
- 🕵️‍♂️ User-specific task management
- 📅 Due date and priority tracking for tasks

## 🚀 Getting Started

### Prerequisites

Make sure you have the following installed:

- Python 3.10+
- PostgreSQL
- pip (Python package installer)
- virtualenv (optional but recommended)

### Installation

1. **Clone the repository:**

    ```sh
    git clone  https://gitlab.rz.htw-berlin.de/s0580270/agile_gruppe_4.git
    cd task_management_tool
    ```

2. **Set up a virtual environment (optional but recommended):**

    ```sh
    python3 -m venv venv
    source venv/bin/activate  # On Windows use `venv\Scripts\activate`
    ```

3. **Install the required packages:**

    ```sh
    pip install -r requirements.txt
    pip install flask
    pip install flask_WTF
    pip install flask_sqlalchemy
    pip install flask_bcrypt
    pip install flask_login
    pip install psycopg2 
    ```

4. **Set up PostgreSQL:**

    - Create a new PostgreSQL database and user:

    ```sql
    CREATE DATABASE flaskapp;
    CREATE USER flaskuser WITH PASSWORD 'flaskpass';
    GRANT ALL PRIVILEGES ON DATABASE flaskapp TO flaskuser;
    ```

5. **Configure environment variables:**

    Create a `.env` file in the project root and add the following variables:

    ```env
    FLASK_APP=app.py
    FLASK_ENV=development
    SQLALCHEMY_DATABASE_URI=postgresql://flaskuser:flaskpass@localhost/flaskapp
    SECRET_KEY=your_secret_key
    ```

6. **Initialize the database:**

    ```sh
    python initialize_db.py
    ```

7. **Run the application:**

    ```sh
    flask run
    ```

8. **Access the application:**

    Open your web browser and go to `http://127.0.0.1:5000`.

## 🛠️ Development

To develop and test the application, follow the steps in the installation section to set up your environment.

### Directory Structure

```plaintext
task_management_tool/
│
├── app.py                  # Main application entry point
├── config.py               # Configuration file
├── models.py               # Database models
├── routes.py               # Application routes
├── forms.py                # Flask-WTF forms
├── templates/              # Jinja2 templates
│   ├── base.html
│   ├── home.html
│   ├── login.html
│   └── register.html
├── static/                 # Static files (CSS, JS, images)
├── initialize_db.py        # Database initialization script
├── requirements.txt        # Python dependencies
└── README.md               # This file

