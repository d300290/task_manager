import React, { useEffect, useState } from 'react';
import axios from '../axiosConfig';
import { Link } from 'react-router-dom';

const Projects = () => {
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    const fetchProjects = async () => {
      const response = await axios.get('/api/projects');
      setProjects(response.data);
      console.log(response.data);
    };

    fetchProjects();
  }, []);

  return (
    <div className="container mx-auto py-10">
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-3xl">Projects</h1>
        <Link to="/projects/create" className="bg-blue-500 text-white px-4 py-2 rounded">
          Create Project
        </Link>
      </div>
      <ul>
        {projects.map(project => (
          <li key={project._id} className="mb-2">
            <Link to={`/projects/${project.id}`} className="text-blue-500 hover:text-blue-700">
              {project.name}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Projects;
