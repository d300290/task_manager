import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import backgroundImage from '../images/background.jpg';  // Import the background image

const Home = () => {
  const navigate = useNavigate();

  useEffect(() => {
    // Check if the user is logged in
    const token = localStorage.getItem('token');
    if (token) {
      navigate('/dashboard');
    }
  }, [navigate]);

  // Define your styles
  const buttonStyle = {
    display: 'inline-block',
    backgroundColor: '#ADD8E6',
    color: '#000',
    borderRadius: '10px',
    padding: '10px 20px',
    textDecoration: 'none',
    margin: '0 10px',
    transition: 'all 0.3s ease',  // This will apply the transition to all properties
    fontWeight: 'bold'  // Make the text thicker
  };

  const textStyle = {
    fontWeight: 'bold',  // Make the text thicker
    transition: 'all 0.3s ease'  // Add a transition effect
  };

  return (
    <section id="showcase" className="relative min-h-screen bg-cover bg-center text-white flex flex-col justify-center items-center" style={{ backgroundImage: `url(${backgroundImage})` }}>
      <div className="absolute inset-0 bg-black opacity-50"></div>
      <div className="relative z-10 text-center">
        <h1 className="text-5xl mb-4 animate-fadeInDown" style={textStyle} onMouseOver={(e) => { e.target.style.transform = 'scale(1.1)'; }} onMouseOut={(e) => { e.target.style.transform = 'scale(1)'; }}>Manage Your Tasks Efficiently</h1>
        <p className="text-xl mb-6 animate-fadeInUp" style={textStyle} onMouseOver={(e) => { e.target.style.transform = 'scale(1.1)'; }} onMouseOut={(e) => { e.target.style.transform = 'scale(1)'; }}>Organize, prioritize, and collaborate with ease.</p>
        <div className="space-x-4">
          <Link to="/register" style={buttonStyle} onMouseOver={(e) => { e.target.style.backgroundColor = '#87CEFA'; e.target.style.transform = 'scale(1.2)'; }} onMouseOut={(e) => { e.target.style.backgroundColor = '#ADD8E6'; e.target.style.transform = 'scale(1)'; }}>Get Started</Link>
          <Link to="/learn-more" style={buttonStyle} onMouseOver={(e) => { e.target.style.backgroundColor = '#87CEFA'; e.target.style.transform = 'scale(1.2)'; }} onMouseOut={(e) => { e.target.style.backgroundColor = '#ADD8E6'; e.target.style.transform = 'scale(1)'; }}>Learn More</Link>
        </div>
      </div>
    </section>
  );
};

export default Home;
