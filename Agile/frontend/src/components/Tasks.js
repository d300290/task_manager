import React, { useEffect, useState } from 'react';
import axios from '../axiosConfig';
import { Link } from 'react-router-dom';

const Tasks = ({ projectId }) => {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    const fetchTasks = async () => {
      try {
        const response = await axios.get(`/api/projects/${projectId}/tasks`);
        setTasks(response.data);
      } catch (error) {
        console.error('Error fetching tasks:', error);
      }
    };

    fetchTasks();
  }, [projectId]);

  return (
    <div className="bg-white shadow rounded-lg p-6">
      <ul>
        {tasks.map((task) => (
          <li key={task._id} className="flex justify-between items-center bg-gray-100 p-2 mb-2 rounded">
            <div>
              <h3 className="text-lg font-semibold">{task.name}</h3>
              <p className="text-gray-700">{task.description}</p>
              {task.dueDate && (
                <p className="text-gray-500 text-sm">Due: {new Date(task.dueDate).toLocaleDateString()}</p>
              )}
            </div>
            <Link
              to={`/projects/${projectId}/tasks/edit/${task._id}`}
              className="bg-yellow-500 hover:bg-yellow-600 text-white px-4 py-2 rounded transition duration-200 ease-in-out"
            >
              Edit Task
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Tasks;
