import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from '../axiosConfig';

const TaskForm = ({ editMode }) => {
  const { projectId, id } = useParams();
  const [task, setTask] = useState({
    name: '',
    description: '',
    status: 'Pending',
    dueDate: '',
  });
  const navigate = useNavigate();

  useEffect(() => {
    if (editMode) {
      const fetchTask = async () => {
        try {
          const response = await axios.get(`/api/tasks/${id}`);
          setTask(response.data);
        } catch (error) {
          console.error('Error fetching task data:', error);
        }
      };

      fetchTask();
    }
  }, [editMode, projectId, id]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setTask((prevTask) => ({ ...prevTask, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (editMode) {
        await axios.put(`/api/tasks/${id}`, task);
      } else {
        await axios.post(`/api/tasks`, {
          ...task,
          projectId,
        });
      }
      navigate(`/projects/${projectId}`);
    } catch (error) {
      console.error('Error saving task data:', error);
    }
  };

  return (
    <div className="container mx-auto py-10 px-4 sm:px-6 lg:px-8">
      <div className="bg-white shadow rounded-lg p-6">
        <h2 className="text-2xl font-semibold text-gray-900 mb-4">
          {editMode ? 'Edit Task' : 'Create Task'}
        </h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label className="block text-gray-700">Name</label>
            <input
              type="text"
              name="name"
              value={task.name}
              onChange={handleChange}
              className="w-full mt-2 p-2 border rounded"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700">Description</label>
            <textarea
              name="description"
              value={task.description}
              onChange={handleChange}
              className="w-full mt-2 p-2 border rounded"
            />
          </div>
          <div className="mb-4">
            <label className="block text-gray-700">Status</label>
            <select
              name="status"
              value={task.status}
              onChange={handleChange}
              className="w-full mt-2 p-2 border rounded"
            >
              <option value="Pending">Pending</option>
              <option value="In Progress">In Progress</option>
              <option value="Completed">Completed</option>
            </select>
          </div>
          <div className="mb-4">
            <label className="block text-gray-700">Due Date</label>
            <input
              type="date"
              name="dueDate"
              value={task.dueDate}
              onChange={handleChange}
              className="w-full mt-2 p-2 border rounded"
            />
          </div>
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded transition duration-200 ease-in-out"
          >
            {editMode ? 'Update Task' : 'Create Task'}
          </button>
        </form>
      </div>
    </div>
  );
};

export default TaskForm;
