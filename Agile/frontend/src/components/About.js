import React from 'react';
import ankitImage from '../images/ankit.jpg';  // Import the image
import '../Styles/about.css';  // Import the CSS file


const About = () => {
  return (
    <section id="about" className="container mx-auto py-10">
      <div className="bg-green-100 p-10 rounded-lg shadow-md">
        <div className="bg-green-300 text-gray-800 p-6 rounded text-center transform transition-transform hover:scale-105">
          <p className="bg-white p-4 rounded shadow-md" style={{ fontWeight: 'bold' }}>
            This is an Agile Software Engineering Project for group 4 at HTW Berlin University
          </p>
        </div>
      </div>
      <div className="card-container">
        <div className="card">
          <div className="card-border-top"></div>
          <div className="img" style={{ backgroundImage: `url(${ankitImage})`, backgroundSize: 'cover' }}></div>
          <span>Ankit Ankit</span>
          <div className="flip-card">
            <div className="flip-card-inner">
              <div className="flip-card-front">
                <button className="button">Click me</button>
              </div>
              <div className="flip-card-back">
                <button className="button" onClick={() => window.open('https://www.linkedin.com/in/ankit4308/', '_blank')}>LinkedIn</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;




