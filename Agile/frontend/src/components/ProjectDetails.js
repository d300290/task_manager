import React, { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';
import axios from '../axiosConfig';
import Tasks from './Tasks';

const ProjectDetails = () => {
  const { id } = useParams();
  const [project, setProject] = useState(null);

  useEffect(() => {
    const fetchProject = async () => {
      try {
        const response = await axios.get(`/api/projects/${id}`);
        setProject(response.data);
      } catch (error) {
        console.error('Error fetching project data:', error);
      }
    };

    fetchProject();
  }, [id]);

  if (!project) return <div className="text-center py-20 text-lg">Loading...</div>;

  return (
    <div className="container mx-auto py-10 px-4 sm:px-6 lg:px-8">
      <div className="bg-white shadow rounded-lg p-6 mb-6">
        <div className="flex justify-between items-center mb-4">
          <div>
            <h1 className="text-3xl font-semibold text-gray-900">{project.name}</h1>
            <p className="text-gray-700 mt-2">{project.description}</p>
          </div>
          <div className="flex space-x-2">
            <Link
              to={`/projects/edit/${id}`}
              className="bg-yellow-500 hover:bg-yellow-600 text-white px-4 py-2 rounded transition duration-200 ease-in-out"
            >
              Edit Project
            </Link>
            <Link
              to={`/projects/${id}/tasks/create`}
              className="bg-green-500 hover:bg-green-600 text-white px-4 py-2 rounded transition duration-200 ease-in-out"
            >
              Create Task
            </Link>
          </div>
        </div>
      </div>
      <div className="bg-white shadow rounded-lg p-6">
        <Tasks projectId={id} />
      </div>
    </div>
  );
};

export default ProjectDetails;
