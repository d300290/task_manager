import React, { useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';

const Header = () => {
  const { isAuthenticated, logout } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleLogout = () => {
    logout();
    navigate('/login');
  };

  // Define your styles
  const linkStyle = {
    display: 'inline-block',
    backgroundColor: '#ADD8E6',
    color: '#000',
    borderRadius: '10px',
    padding: '10px 20px',
    textDecoration: 'none',
    margin: '0 10px',
    transition: 'all 0.3s ease',  // This will apply the transition to all properties
    fontWeight: 'bold'  // Make the text bold
  };

  const titleStyle = {
    fontWeight: 'bold'  // Make the text bold
  };

  return (
    <header className="bg-gray-800 text-white py-4">
      <div className="container mx-auto flex justify-between items-center">
        <div>
          <h1 className="text-3xl" style={titleStyle}>OTMT</h1>
          <h2 className="text-xl text-gray-300" style={titleStyle}>Online Task Management Tool</h2>
        </div>
        <nav>
          <ul className="flex space-x-4">
            <li>
              <Link to={isAuthenticated ? "/dashboard" : "/"} style={linkStyle} 
                onMouseOver={(e) => { e.target.style.backgroundColor = '#87CEFA'; e.target.style.transform = 'scale(1.1)'; }} 
                onMouseOut={(e) => { e.target.style.backgroundColor = '#ADD8E6'; e.target.style.transform = 'scale(1)'; }}>
                {isAuthenticated ? "Dashboard" : "Home"}
              </Link>
            </li>
            <li>
              <Link to="/about" style={linkStyle} 
                onMouseOver={(e) => { e.target.style.backgroundColor = '#87CEFA'; e.target.style.transform = 'scale(1.1)'; }} 
                onMouseOut={(e) => { e.target.style.backgroundColor = '#ADD8E6'; e.target.style.transform = 'scale(1)'; }}>
                About
              </Link>
            </li>
            {isAuthenticated ? (
              <>
                <li>
                  <button onClick={handleLogout} style={linkStyle} 
                    onMouseOver={(e) => { e.target.style.backgroundColor = '#87CEFA'; e.target.style.transform = 'scale(1.1)'; }} 
                    onMouseOut={(e) => { e.target.style.backgroundColor = '#ADD8E6'; e.target.style.transform = 'scale(1)'; }}>
                    Logout
                  </button>
                </li>
              </>
            ) : (
              <>
                <li>
                  <Link to="/login" style={linkStyle} 
                    onMouseOver={(e) => { e.target.style.backgroundColor = '#87CEFA'; e.target.style.transform = 'scale(1.1)'; }} 
                    onMouseOut={(e) => { e.target.style.backgroundColor = '#ADD8E6'; e.target.style.transform = 'scale(1)'; }}>
                    Login
                  </Link>
                </li>
                <li>
                  <Link to="/register" style={linkStyle} 
                    onMouseOver={(e) => { e.target.style.backgroundColor = '#87CEFA'; e.target.style.transform = 'scale(1.1)'; }} 
                    onMouseOut={(e) => { e.target.style.backgroundColor = '#ADD8E6'; e.target.style.transform = 'scale(1)'; }}>
                    Register
                  </Link>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
