import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from '../axiosConfig';
import { FaPlus, FaEdit } from 'react-icons/fa';

const Dashboard = () => {
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchProjects = async () => {
      try {
        const response = await axios.get('/api/projects');
        setProjects(response.data);
      } catch (error) {
        setError('Error fetching projects');
        console.error('Error fetching projects:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchProjects();
  }, []);

  const loaderStyle = {
    borderTopColor: '#3498db',
    animation: 'spinner 1.5s linear infinite',
    border: '8px solid #f3f3f3',
    borderRadius: '50%',
    borderTop: '8px solid #3498db',
    width: '64px',
    height: '64px',
  };

  return (
    <div className="min-h-screen bg-gray-100 py-10 px-4">
      <div className="container mx-auto">
        <div className="flex justify-between items-center mb-8">
          <h1 className="text-4xl font-bold text-gray-800">Dashboard</h1>
          <div className="space-x-4">
            <Link 
              to="/projects/create" 
              className="inline-block bg-blue-500 text-white rounded-lg py-2 px-4 transition-transform transform hover:bg-blue-600 hover:scale-105 font-bold"
            >
              <FaPlus className="mr-2" />
              Create Project
            </Link>
          </div>
        </div>

        {loading ? (
          <div className="flex justify-center items-center h-64">
            <div style={loaderStyle}></div>
          </div>
        ) : error ? (
          <div className="flex justify-center items-center h-64">
            <p className="text-red-500 font-bold">{error}</p>
          </div>
        ) : (
          <div>
            <h2 className="text-2xl font-bold mb-4 text-gray-800">Projects</h2>
            {projects.length > 0 ? (
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {projects.map(project => (
                  <div 
                    key={project.id} 
                    className="bg-white p-6 rounded-lg shadow-lg transition-transform transform hover:scale-105"
                  >
                    <Link 
                      to={`/projects/${project._id}`} 
                      className="text-blue-500 hover:text-blue-700 font-semibold text-lg block mb-4"
                    >
                      {project.name}
                    </Link>
                    <Link 
                      to={`/projects/edit/${project._id}`} 
                      className="bg-yellow-500 hover:bg-yellow-700 text-white py-1 px-3 rounded inline-flex items-center"
                    >
                      <FaEdit className="mr-2" />
                      Edit
                    </Link>
                  </div>
                ))}
              </div>
            ) : (
              <p className="text-gray-700 font-bold">No projects available. Start by creating one!</p>
            )}
          </div>
        )}
      </div>

      <style>{`
        @keyframes spinner {
          0% {
            transform: rotate(0deg);
          }
          100% {
            transform: rotate(360deg);
          }
        }
      `}</style>
    </div>
  );
};

export default Dashboard;
