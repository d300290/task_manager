import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import '../Styles/projectform.css'; // Import your CSS file
import axios from '../axiosConfig';

const ProjectForm = ({ editMode = false }) => {
  const { id } = useParams();
  console.log(id); // This should now log the correct project id
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [priority, setPriority] = useState('');
  const [showForm, setShowForm] = useState(false); // State to manage animation
  const navigate = useNavigate();

  useEffect(() => {
    setShowForm(true); // Triggering fadeIn animation on component mount

    if (editMode) {
      axios.get(`/api/projects/${id}`)
        .then(response => {
          const { name, description, startDate, endDate, priority } = response.data;
          setName(name);
          setDescription(description);
          setStartDate(startDate.split('T')[0]); // Format date
          setEndDate(endDate.split('T')[0]); // Format date
          setPriority(priority);
        })
        .catch(error => {
          console.error('Error fetching project:', error.response || error.message);
          alert('Error fetching project details');
        });
    }
  }, [editMode, id]);

  // Handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();
    const project = { name, description, startDate, endDate, priority };
    console.log('Submitting project:', project); // Log project data before sending
    try {
      if (editMode) {
        const response = await axios.put(`/api/projects/${id}`, project);
        console.log('Response:', response.data); // Log response data
        alert('Project updated successfully');
      } else {
        const response = await axios.post('/api/projects', project);
        console.log('Response:', response.data); // Log response data
        alert('Project created successfully');
      }
      navigate('/dashboard');
    } catch (error) {
      console.error('Error creating/updating project:', error.response || error.message); // Log detailed error
      alert('Error creating/updating project');
    }
  };

  return (
    <div className="form-container">
      {/* Background balls animation */}
      <div className="background-animation">
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
        <div className="ball"></div>
      </div>

      <div className="form-content">
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="name">Project Name</label>
            <input
              type="text"
              id="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <textarea
              id="description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              required
            ></textarea>
          </div>
          <div className="form-group">
            <label htmlFor="start-date">Start Date</label>
            <input
              type="date"
              id="start-date"
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="end-date">End Date</label>
            <input
              type="date"
              id="end-date"
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="priority">Priority</label>
            <select
              id="priority"
              value={priority}
              onChange={(e) => setPriority(e.target.value)}
              required
            >
              <option value="" disabled>Select Priority</option>
              <option value="low">Low</option>
              <option value="medium">Medium</option>
              <option value="high">High</option>
            </select>
          </div>
          <div className="form-group">
            <button type="submit" className="create-button">
              {editMode ? 'Update Project' : 'Create Project'}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ProjectForm;
