const Team = require('../models/team');

exports.getAllTeams = async (req, res) => {
  try {
    const teams = await Team.find().populate('members');
    res.json(teams);
  } catch (err) {
    res.status(500).send('Server error');
  }
};

exports.getTeamById = async (req, res) => {
  try {
    const team = await Team.findById(req.params.id).populate('members');
    if (!team) return res.status(404).send('Team not found');
    res.json(team);
  } catch (err) {
    res.status(500).send('Server error');
  }
};

exports.createTeam = async (req, res) => {
  try {
    const team = new Team({
      ...req.body,
    });
    await team.save();
    res.json({ success: true, message: 'Team created successfully' });
  } catch (err) {
    res.status(500).send('Server error');
  }
};

exports.updateTeam = async (req, res) => {
  try {
    const team = await Team.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    );
    if (!team) return res.status(404).send('Team not found');
    res.json({ success: true, message: 'Team updated successfully' });
  } catch (err) {
    res.status(500).send('Server error');
  }
};

exports.deleteTeam = async (req, res) => {
  try {
    const team = await Team.findOneAndRemove({ _id: req.params.id });
    if (!team) return res.status(404).send('Team not found');
    res.json({ success: true, message: 'Team deleted successfully' });
  } catch (err) {
    res.status(500).send('Server error');
  }
};
