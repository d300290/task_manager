const Project = require('../models/project');
const Task = require('../models/task');
const Team = require('../models/team');

exports.getAllProjects = async (req, res) => {

  try {
        console.log('User from auth middleware:', req.user);

    const projects = await Project.find({ userId: req.user.id });
    res.json(projects);
  } catch (err) {
    console.error('Error getting all projects:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
};

exports.getProjectById = async (req, res) => {
  try {
    console.log('User from auth middleware:', req.user);
    const project = await Project.findOne({ _id: req.params.id, userId: req.user.id });
    if (!project) return res.status(404).json({ success: false, message: 'Project not found' });
    res.json(project);
  } catch (err) {
    console.error('Error getting project by ID:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
};

exports.createProject = async (req, res) => {
  try {
    const { name, description, startDate, endDate, priority } = req.body;

    console.log('User from auth middleware:', req.user);

    if (!name || !description || !startDate || !endDate || !priority) {
      return res.status(400).json({ success: false, message: 'All fields are required' });
    }

    const project = new Project({
      name,
      description,
      startDate,
      endDate,
      priority,
      userId: req.user.id
    });

    await project.save();
    res.status(201).json({ success: true, message: 'Project created successfully', project });
  } catch (err) {
    console.error('Error creating project:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
};

exports.updateProject = async (req, res) => {
  try {
    const { name, description, startDate, endDate, priority } = req.body;

    if (!name || !description || !startDate || !endDate || !priority) {
      return res.status(400).json({ success: false, message: 'All fields are required' });
    }

    const project = await Project.findOneAndUpdate(
      { _id: req.params.id, userId: req.user.id },
      { name, description, startDate, endDate, priority },
      { new: true }
    );

    if (!project) return res.status(404).json({ success: false, message: 'Project not found' });

    res.json({ success: true, message: 'Project updated successfully', project });
  } catch (err) { 
    console.error('Error updating project:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
};

exports.deleteProject = async (req, res) => {
  try {
    const project = await Project.findOneAndRemove({ _id: req.params.id, userId: req.user.id });
    if (!project) return res.status(404).json({ success: false, message: 'Project not found' });
    res.json({ success: true, message: 'Project deleted successfully' });
  } catch (err) {
    console.error('Error deleting project:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
};

exports.getTasksForProject = async (req, res) => {
  try {
    const tasks = await Task.find({ projectId: req.params.id, userId: req.user.id });
    res.json(tasks);
  } catch (err) {
    console.error('Error getting tasks for project:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
};

