const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const projectSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: true },
  priority: { type: String, required: true },
  userId: { type: Schema.Types.ObjectId, ref: 'User', required: true } 
});

const Project = mongoose.model('Project', projectSchema);

module.exports = Project;
